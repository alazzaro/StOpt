# Copyright (C) 2018 The Regents of the University of California, Michael Ludkovski and Aditya Maheshwari
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)

from __future__ import division
import numpy as np

class basicVariables:


    def __init__(self, maturity=10):

        self.K = 10 #switching cost
        

        self.meshX = 4 #number of meshes on demand dimension
        self.meshI = 10 #number of meshes on inventory dimension

        self.I0=5 #initial inventory

        self.I_minMax = [0,10] #Level of charge of the battery min max in KW
        self.B_minMax =  [-6,6] #battery min max output, positive implies output and negative implies input
        self.d_minMax =  [1,10] #battery min max output, positive implies output and negative implies input


        self.H = 1 # represents the number of regimes which in this case is 2, as a result H=1

        # assuming the cost function is of the type 
        # c1*d^a + c2*1{st>0} + c3*st*1{st<0}
        c =[1,1000000,5] # assuming the cost function is of the type , c[0]*d^a + c[1]*1{st>0} + c[2]*1{st<0}(-st)
        self.a = 0.9
        self.c1 = c[0]
        self.c2 = c[1]
        self.c3 = c[2]


        self.maturity = maturity #unit in hours
        self.dt = 15/60 #unit in hours
        self.nstep = int(self.maturity/self.dt) #number of macro steps

        self.controlType = 'notBangBang' #'BangBang' #

        if self.controlType  != 'BangBang':
            self.steps_d = 10   # discretization of diesel engine output
            self.discrertize_d = np.hstack((0,np.linspace(self.d_minMax[0],self.d_minMax[1],self.steps_d)))[:,None].T

        # demand parameters
        self.lambd = 0.5 #mean reversion rate
        self.sigma = 2 #volatility
        self.ampl = 6 
        self.mu = 0


        self.DemandUB = 10

        self.rmctype = 'regress now 2D' #'gd' #
        
        if self.rmctype == 'gd':
            self.regType = 'piecewiseLinear'
            # number of simulations
            self.nbsimulOpt = 500*self.meshX

        elif self.rmctype == 'regress now 2D':
            self.regType =  'globalPolynomial' #'kernel' #'piecewiseLinear' #
            self.nbsimulOpt = 10000

            self.degree= 2  # used only with regType = 'globalPolynomial'

            # used only with regType = 'kernel'
            self.bandwidth = 0.2
            # factor for the number of points
            self.factPoint=1.

        self.filetoDump = "condExp_rmcType_"+self.rmctype+"_regType_" + self.regType+"_controlType_"+self.controlType



if __name__ == "__main__":

    param = basicVariables()
    print(param.B_minMax," " , param.I_minMax," ", param.K," ", param.nbsimulOpt)

    # for i in range(1,len(param.discrertize_d)):
    #     print param.discrertize_d[i]
