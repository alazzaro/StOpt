# Copyright (C) 2020 EDF
# Copyright (C) 2020 CSIRO Data61
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)
import numpy as np
import unittest
import random

# unit test for Laplacian  kernel regression 
############################################

class testLaplacianKernelRegression(unittest.TestCase):

    # One dimensional test
    def testRegressionConst1D(self):
        import StOptReg 
        nbSimul = 100000;
        np.random.seed(1000)
        x = np.random.uniform(-1.,1.,size=(1,nbSimul));
        # real function
        toReal = (4+x[0,:]+(3+x[0,:])*(2+x[0,:]))
        # function to regress
        toRegress = toReal + 4*np.random.normal(0.,1,nbSimul)
        # bandwidth
        bandwidth = 0.05*np.ones(1)
        # multiplicative for rectilinear grid
        coeffMul = 2
        # Linear or constant regressions
        bLinear= False
        # Regressor
        regressor = StOptReg.LaplacianGridKernelRegression(False,x,bandwidth, coeffMul,bLinear)
        # nb simul
        nbSimul= regressor.getNbSimul()
        # particles
        part = regressor.getParticles()
        # test particules
        y = regressor.getAllSimulations(toRegress).transpose()
        # compare to real value
        diff = max(abs(y-toReal)/y)
        self.assertAlmostEqual(diff,0., 1,"Difference between function and its condition expectation estimated greater than tolerance")

    def testRegressionLinear1D(self):
        import StOptReg 
        nbSimul = 100000;
        np.random.seed(1000)
        x = np.random.uniform(-1.,1.,size=(1,nbSimul));
        # real function
        toReal = (4+x[0,:]+(3+x[0,:])*(2+x[0,:]))
        # function to regress
        toRegress = toReal + 4*np.random.normal(0.,1,nbSimul)
        # bandwidth
        bandwidth = 0.05*np.ones(1)
        # multiplicative for rectilinear grid
        coeffMul = 2
        # Linear or constant regressions
        bLinear= True
        # Regressor
        regressor = StOptReg.LaplacianGridKernelRegression(False,x,bandwidth, coeffMul,bLinear)
        # nb simul
        nbSimul= regressor.getNbSimul()
        # particles
        part = regressor.getParticles()
        # test particules
        y = regressor.getAllSimulations(toRegress).transpose()
        # compare to real value
        diff = max(abs(y-toReal)/y)
        self.assertAlmostEqual(diff,0., 1,"Difference between function and its condition expectation estimated greater than tolerance")

    # two dimensonnal test
    def testRegressionConstKernel(self):
        import StOptReg 
        np.random.seed(1000)
        nbSimul = 400000;
        x = np.random.uniform(-1.,1.,size=(2,nbSimul));
        # real function
        toReal = (4+x[0,:])*(3+x[1,:])
        # function to regress
        toRegress = toReal + np.random.normal(0.,1,nbSimul)
        # bandwidth
        bandwidth = 0.05*np.ones(2)
        # multiplicative for rectilinear grid
        coeffMul = 2
        # Linear or constant regressions
        bLinear= False
        # Regressor
        regressor = StOptReg.LaplacianGridKernelRegression(False,x,bandwidth, coeffMul,bLinear)
        # test
        y = regressor.getAllSimulations(toRegress).transpose()
        # particles
        part = regressor.getParticles()
        # compare to real value
        diff = max(abs(y-toReal)/toReal)
        self.assertAlmostEqual(diff,0., 1,"Difference between function and its condition expectation estimated greater than tolerance")

    # two dimensonnal test
    def testRegressionLinearKernel(self):
        import StOptReg 
        np.random.seed(1000)
        nbSimul = 400000;
        x = np.random.uniform(-1.,1.,size=(2,nbSimul));
        # real function
        toReal = (4+x[0,:])*(3+x[1,:])
        # function to regress
        toRegress = toReal + np.random.normal(0.,1,nbSimul)
        # bandwidth
        bandwidth = 0.05*np.ones(2)
        # multiplicative for rectilinear grid
        coeffMul = 2
        # Linear or constant regressions
        bLinear= True
        # Regressor
        regressor = StOptReg.LaplacianGridKernelRegression(False,x,bandwidth, coeffMul,bLinear)
        y = regressor.getAllSimulations(toRegress).transpose()
        # particles
        part = regressor.getParticles()
        # compare to real value
        diff = max(abs(y-toReal)/toReal)
        self.assertAlmostEqual(diff,0., 1,"Difference between function and its condition expectation estimated greater than tolerance")

    # test different second member
    def testSecondMemberConstKernel(self):
         import StOptReg 
         np.random.seed(1000)
         nbSimul = 100;
         x = np.random.uniform(-1.,1.,size=(1,nbSimul));
         toReal = np.array([((2+x[0,:]+(1+x[0,:])*(1+x[0,:]))).tolist(),((3+x[0,:]+(2+x[0,:])*(2+x[0,:]))).tolist()])
         # function to regress
         toRegress = np.array([(toReal[0,:] + 4*np.random.normal(0.,1,nbSimul)).tolist(),(toReal[1,:] + 4*np.random.normal(0.,1,nbSimul)).tolist()])
         # bandwidth
         bandwidth = 0.1*np.ones(1)
         # multiplicative for rectilinear grid
         coeffMul = 2
         # Linear or constant regressions
         bLinear= False
         # Regressor
         regressor = StOptReg.LaplacianGridKernelRegression(False,x,bandwidth, coeffMul,bLinear)
         y = regressor.getAllSimulationsMultiple(toRegress)
         # check 
         z1 = regressor.getAllSimulations(toRegress[0,:]).transpose()
         z2 = regressor.getAllSimulations(toRegress[1,:]).transpose()
         # compare to real value
         diff = max(abs(y[0,:]-z1[:] ))+max(abs(y[1,:]-z2[:] ))
         self.assertAlmostEqual(diff,0., 7,"Difference between function and its condition expectation estimated greater than tolerance")

    def testSecondMemberLinearKernel(self):
         import StOptReg 
         np.random.seed(1000)
         nbSimul = 100;
         x = np.random.uniform(-1.,1.,size=(1,nbSimul));
         toReal = np.array([((2+x[0,:]+(1+x[0,:])*(1+x[0,:]))).tolist(),((3+x[0,:]+(2+x[0,:])*(2+x[0,:]))).tolist()])
         # function to regress
         toRegress = np.array([(toReal[0,:] + 4*np.random.normal(0.,1,nbSimul)).tolist(),(toReal[1,:] + 4*np.random.normal(0.,1,nbSimul)).tolist()])
         # bandwidth
         bandwidth = 0.1*np.ones(1)
         # multiplicative for rectilinear grid
         coeffMul = 2
         # Linear or constant regressions
         bLinear= True
         # Regressor
         regressor = StOptReg.LaplacianGridKernelRegression(False,x,bandwidth, coeffMul,bLinear)
         y = regressor.getAllSimulationsMultiple(toRegress)
         # check 
         z1 = regressor.getAllSimulations(toRegress[0,:]).transpose()
         z2 = regressor.getAllSimulations(toRegress[1,:]).transpose()
         # compare to real value
         diff = max(abs(y[0,:]-z1[:] ))+max(abs(y[1,:]-z2[:] ))
         self.assertAlmostEqual(diff,0., 7,"Difference between function and its condition expectation estimated greater than tolerance")


    # test  date 0
    def testDateZeroConstKernel(self):
        import StOptReg 
        nbSimul = 50000;
        np.random.seed(1000)
        x = np.random.uniform(-1.,1.,size=(1,nbSimul));
        # real function
        toReal = (2+x[0,:]+(1+x[0,:])*(1+x[0,:]))
        # function to regress
        toRegress = toReal + 4*np.random.normal(0.,1,nbSimul)
        # bandwidth
        bandwidth = 0.05*np.ones(1)
        # multiplicative for rectilinear grid
        coeffMul = 2
        # Linear or constant regressions
        bLinear= False
        # Regressor
        regressor = StOptReg.LaplacianGridKernelRegression(True,x,bandwidth, coeffMul,bLinear)
        y = regressor.getAllSimulations(toRegress).mean()
        self.assertAlmostEqual(y,toRegress.mean(),8,"Not equality by average")

    def testDateZeroLinearKernel(self):
        import StOptReg 
        nbSimul = 50000;
        np.random.seed(1000)
        x = np.random.uniform(-1.,1.,size=(1,nbSimul));
        # real function
        toReal = (2+x[0,:]+(1+x[0,:])*(1+x[0,:]))
        # function to regress
        toRegress = toReal + 4*np.random.normal(0.,1,nbSimul)
        # bandwidth
        bandwidth = 0.05*np.ones(1)
        # multiplicative for rectilinear grid
        coeffMul = 2
        # Linear or constant regressions
        bLinear= True
        # Regressor
        regressor = StOptReg.LaplacianGridKernelRegression(True,x,bandwidth, coeffMul,bLinear)
        y = regressor.getAllSimulations(toRegress).mean()
        self.assertAlmostEqual(y,toRegress.mean(),8,"Not equality by average")
        
if __name__ == '__main__': 
    unittest.main()
