# Copyright (C) 2020 EDF
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)
import numpy as np
import unittest

# unit test for fast CDF
#########################

class testFastCDFOnSample(unittest.TestCase):
    def test2D(self):
        import StOptCDF
        
        nbSimul = 10000
        x = np.random.normal(size=(2,nbSimul));
        y =  np.ones([nbSimul])

        #  CDF at sample points
        cdf= StOptCDF.fastCDFOnSample(x,y)
        
        print("CDF", cdf[:10], "And " , cdf[-10:])
if __name__ == '__main__': 
    unittest.main()
