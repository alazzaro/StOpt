# Copyright (C) 2016 EDF
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)
import numpy as np
import unittest
import random
import math
import StOptGrids
 
# unit test for regular grids
############################

class testGrids(unittest.TestCase):

    # 3 dimensional test for linear interpolation on regular grids
    def testRegularGrids(self):
        # low value for the meshes
        lowValues =np.array([1.,2.,3.],dtype=np.float)
        # size of the meshes
        step = np.array([0.7,2.3,1.9],dtype=np.float)
        # number of steps
        nbStep = np.array([4,5,6], dtype=np.int32)
        # create the regular grid
        grid = StOptGrids.RegularSpaceGrid(lowValues,step,nbStep)
        iterGrid = grid.getGridIterator()
        # array to store
        data = np.empty(grid.getNbPoints())
        # iterates on points and store values
        while( iterGrid.isValid()):
          #get coordinates of the point
          pointCoord = iterGrid.getCoordinate()
          data[iterGrid.getCount()] = math.log(1. + pointCoord.sum())
          iterGrid.next()
        # get back an interpolator
        ptInterp = np.array([2.3,3.2,5.9],dtype=np.float)
        interpol = grid.createInterpolator(ptInterp)
        # calculate interpolated value
        interpValue = interpol.apply(data)
        print(("Interpolated value" , interpValue))
        # test grids function
        iDim = grid.getDimension()
        pt = grid.getExtremeValues()

if __name__ == '__main__': 
    unittest.main()
