// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef ONEDIMDATAWRAP_H
#define  ONEDIMDATAWRAP_H
#include "memory"
#include "StOpt/core/grids/OneDimSpaceGrid.h"
#include "StOpt/core/grids/OneDimData.h"
#include "StOpt/python/Pybind11VectorAndList.h"

/**  \file OneDimDataWrap.h
 *  Wrapping
 * \author Xavier Warin
 */


/// \class RegimeCurve  OneDimDataWrap.h
/// Wrapping for curve
class RegimeCurve : public StOpt::OneDimData<StOpt::OneDimSpaceGrid, int>
{
public :
    /// \brief Constructor
    /// \param time    \f$x\f$ variable
    /// \param values  \f$y\f$ such that \f$y = f(x)\f$
    RegimeCurve(const std::shared_ptr<StOpt::OneDimSpaceGrid> &time, const pybind11::list &values): StOpt::OneDimData<StOpt::OneDimSpaceGrid, int>(time, convertFromListToShared<int>(values)) {}
};

#endif
