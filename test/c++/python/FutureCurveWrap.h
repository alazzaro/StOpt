// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef FUTURECURVEWRAP_H
#define  FUTURECURVEWRAP_H
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include "StOpt/core/grids/OneDimRegularSpaceGrid.h"
#include "StOpt/core/grids/OneDimData.h"
#include "StOpt/python/Pybind11VectorAndList.h"

/** \file FutureCurveWrap.h
 * \brief Map a curve for futures
 * \author Xavier Warin
 */

/// \class FutureCurve FutureCurveWrap.h
/// Future curve wrap
class FutureCurve : public  StOpt::OneDimData< StOpt::OneDimRegularSpaceGrid, double>
{
public :
    /// \brief constructor
    /// \param time    \f$x\f$  values
    /// \param values   \f$y\f$ values associated
    FutureCurve(const std::shared_ptr<StOpt::OneDimRegularSpaceGrid> &time, const pybind11::list &values):  StOpt::OneDimData<StOpt::OneDimRegularSpaceGrid, double>(time, convertFromListToShared<double>(values)) {}
};

#endif
