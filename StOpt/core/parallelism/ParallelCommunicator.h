
#ifndef PARALLELCOMMUNICATOR_H
#define PARALLELCOMMUNICATOR_H
#ifdef USE_MPI
#include <boost/mpi.hpp>
#endif
#include <memory>

// Use some stub values when MPI is not used
#ifndef USE_MPI
using MPI_Comm = int ;
const MPI_COMM_NULL = 0;
#endif

namespace StOpt
{

  class ParallelCommunicator {

  public:

    ParallelCommunicator(const MPI_Comm &comm);
    virtual ~ParallelCommunicator() {}

#ifdef USE_MPI
    inline const boost::mpi::communicator& getCommunicator() const
    {
      return *m_communicator;
    }
#endif

  private:
#ifdef USE_MPI
    std::unique_ptr<boost::mpi::communicator> m_communicator;
#endif

    // No copy and move constructor
    ParallelCommunicator(const ParallelCommunicator&) = delete;
    ParallelCommunicator(ParallelCommunicator&&) = delete;
  };
}
#endif // PARALLELCOMMUNICATOR_H
