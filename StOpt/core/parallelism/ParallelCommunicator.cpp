#include "StOpt/core/parallelism/ParallelCommunicator.h"

namespace StOpt
{
  ParallelCommunicator::ParallelCommunicator([[maybe_unused]] const MPI_Comm &comm)
#ifdef USE_MPI
    : m_communicator{std::make_unique<boost::mpi::communicator>(comm, boost::mpi::comm_attach)}
#endif // USE_MPI
  {}
}
