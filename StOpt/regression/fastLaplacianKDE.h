// Copyright (C) 2020 EDF
// Copyright (C) 2020 CSIRO Data61
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef FASTLAPLACIANKDE_H
#define FASTLAPLACIANKDE_H
#include <vector>
#include <array>
#include <memory>
#include <iostream>

/** \file fastLaplacianKDE.h
 *  \brief Permits to calculate  KDE type expression for Laplacian kernel
 *  Algorithm by Langrené Warin 2020 for fast summation
 * \author  Xavier Warin, Nicolas Langrené
 */



/// \brief Calculate all terms for kernel regressions
/// \param p_x  samples   (dimension, nbSim)
/// \param p_z  rectilinear points
/// \param p_h  window size per direction
/// \param p_y  value to regress  (nb function to regress, nb samples)
Eigen::ArrayXXd  FastSumRegressionTerms(const Eigen::ArrayXXd &p_x,
                                        const std::vector< std::shared_ptr<Eigen::ArrayXd> >    &p_z,
                                        const Eigen::ArrayXd &p_h,
                                        const Eigen::ArrayXXd &p_y);

#endif
