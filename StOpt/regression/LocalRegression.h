// Copyright (C) 2016, 2017 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef LOCALREGRESSION_H
#define LOCALREGRESSION_H
#include <vector>
#include <memory>
#include <array>
#include <Eigen/Dense>
#include "StOpt/regression/BaseRegression.h"
#include "StOpt/regression/LocalAdaptCellRegression.h"
#include "StOpt/regression/meshCalculationLocalRegression.h"
#include "StOpt/core/grids/InterpolatorSpectral.h"

/** \file   LocalRegression.h
 *  \brief  Base class for local regressions with adapted mesh size.
 *          See for the linear version the  article "Monte-Carlo valorisation of American options: facts and new algorithms to improve existing methods"
 *          by Bouchard, Warin in "Numerical methods in finance", Springer,2012
 *  \author Xavier Warin
 */
namespace StOpt
{
/**
 * \defgroup Local Piecewise regression
 * \brief It implements local local regression
  *@{
 */
/// \class LocalRegression LocalRegression.h
/// To be used in Monte Carlo methods  regression on each cell which is constructed such that each cell has
/// roughly the same number of particles
class LocalRegression : public LocalAdaptCellRegression
{
protected:
    std::vector< std::shared_ptr <Eigen::ArrayXd>  > m_mesh1D ; ///< Mesh representation per dimension

    /// \brief To a particle affect to cell number
    /// \param p_oneParticle  One point
    /// \return cell number
    int  particleToMesh(const Eigen::ArrayXd &p_oneParticle) const;

public :

    /// \brief Default ructor
    LocalRegression() {}

    /// \brief Constructor
    /// \param  p_nbMesh       discretization in each direction
    /// \param  p_bRotationAndRecale do we use SVD
    LocalRegression(const Eigen::ArrayXi   &p_nbMesh, const bool &p_bRotationAndRecale);

    /// \brief Constructor for object constructed at each time step
    /// \param  p_bZeroDate          first date is 0?
    /// \param  p_particles          particles used for the meshes.
    ///                              First dimension  : dimension of the problem,
    ///                              second dimension : the  number of particles
    /// \param  p_nbMesh             discretization in each direction
    /// \param  p_bRotationAndRecale do we use SVD
    LocalRegression(const bool &p_bZeroDate,
                    const Eigen::ArrayXXd  &p_particles,
                    const Eigen::ArrayXi   &p_nbMesh,
                    const bool &p_bRotationAndRecale);

    /// \brief Second constructor , only to be used in simulation
    LocalRegression(const   bool &p_bZeroDate,
                    const   Eigen::ArrayXi &p_nbMesh,
                    const   Eigen::Array< std::array< double, 2>, Eigen::Dynamic, Eigen::Dynamic >   &p_mesh,
                    const   std::vector< std::shared_ptr< Eigen::ArrayXd > > &p_mesh1D,
                    const   Eigen::ArrayXd &p_meanX,
                    const   Eigen::ArrayXd   &p_etypX,
                    const   Eigen::MatrixXd   &p_svdMatrix,
                    const   bool &p_bRotationAndRecale) ;

    /// \brief Copy constructor
    /// \param p_object object to copy
    LocalRegression(const LocalRegression   &p_object);

    inline const std::vector< std::shared_ptr< Eigen::ArrayXd > > &getMesh1D() const
    {
        return m_mesh1D;
    }
    /// \brief Get some local accessors with copy (useful for python)
    ///@{

    inline std::vector< std::shared_ptr< Eigen::ArrayXd > > getMesh1DCopy() const
    {
        return m_mesh1D;
    }
    ///@}
};
/**@}*/
}
#endif /*LOCALREGRESSION_H*/
