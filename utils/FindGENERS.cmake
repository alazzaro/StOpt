# find geners library
# This module finds geners if it is installed and determines where the include files are.
# It sets the following variables
# GENERS_FOUND          - have the GENERS  LIBS been found
# GENERS_LIBRARIES       - Geners libraries
# GENERS_INCLUDE_DIRS   -path where to find geners include

# module directory
set( GENERS_ROOT GENERS_ROOT-NOT-FOUND CACHE PATH
  "Dependance geners . Please set GENERS_ROOT variables.")
if (NOT GENERS_ROOT)
  message(FATAL_ERROR
    "Geners directory not found. Please set the variable 'GENERS_ROOT'.")
endif(NOT GENERS_ROOT)

if (MSVC AND (CMAKE_GENERATOR MATCHES "^Visual.*Win64$"))
    set (BUILD build/x64/)
else (MSVC AND (CMAKE_GENERATOR MATCHES "^Visual.*Win64$"))
    set (BUILD build/)
endif (MSVC AND (CMAKE_GENERATOR MATCHES "^Visual.*Win64$"))


FIND_LIBRARY(GENERS_LIBRARY
  NAMES geners
  PATHS
  ${GENERS_LIBRARIES}
  ${GENERS_ROOT}/${BUILD}
  ${GENERS_ROOT}/${BUILD}/Release
  ${GENERS_ROOT}/${BUILD}/RelWithDebInfo
  ${GENERS_ROOT}/lib
  )


FIND_PATH(GENERS_INCLUDE_DIR
    NAMES GenericIO.hh
    PATHS
    ${GENERS_ROOT}
    ${GENERS_INCLUDE_DIR}
    ${GENERS_INCLUDE_DIRS}
    )

MARK_AS_ADVANCED(
  GENERS_LIBRARY
  GENERS_INCLUDE_DIR
  )

SET(GENERS_INCLUDE_DIRS "${GENERS_INCLUDE_DIR}")
SET(GENERS_LIBRARIES "${GENERS_LIBRARY}")
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Geners DEFAULT_MSG  GENERS_LIBRARIES GENERS_INCLUDE_DIRS)
